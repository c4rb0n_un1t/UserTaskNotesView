#include "tasksketchmanagerview.h"
#include "ui_form.h"

#include <QFileDialog>

TaskSketchManagerView::TaskSketchManagerView() :
	QObject(),
	PluginBase(this),
	ui(new Ui::Form),
	m_elementBase(new GUIElementBase(this, {"MainMenuItem"}))
{
	ui->setupUi(m_elementBase);

	initPluginBase({
		{INTERFACE(IPlugin), this},
		{INTERFACE(IGUIElement), m_elementBase}
	},
	{
		{INTERFACE(IUserTaskNotesDataExtention), myModel}
	});
	m_elementBase->initGUIElementBase();

	connect(ui->buttonClose, SIGNAL(clicked(bool)), SLOT(buttonClose_clicked()));
	connect(ui->treeView, &QTreeView::entered, this, &TaskSketchManagerView::onTreeViewClicked);
	connect(ui->treeView, &QTreeView::activated, this, &TaskSketchManagerView::onTreeViewClicked);
	connect(ui->treeView, &QTreeView::clicked, this, &TaskSketchManagerView::onTreeViewClicked);
	connect(ui->treeView, &QTreeView::pressed, this, &TaskSketchManagerView::onTreeViewClicked);

	connect(ui->pushButtonAdd, &QPushButton::clicked, this, &TaskSketchManagerView::saveText);
	connect(ui->pushButtonRevert, &QPushButton::clicked, this, &TaskSketchManagerView::revertText);
	connect(ui->textEdit, &QTextEdit::textChanged, this, [=]() {
		ui->pushButtonAdd->setEnabled(true);
		ui->pushButtonRevert->setEnabled(true);
	});
}

void TaskSketchManagerView::onReady()
{
	m_filterUserTasks = myModel->getModel()->getFilter();
	m_filterUserTasks->setColumns({
		{INTERFACE(IUserTaskDataExtention), {"name"}}
	});
	ui->treeView->setModel(m_filterUserTasks);
}

void TaskSketchManagerView::buttonClose_clicked()
{
	m_elementBase->closeSelf();
}

void TaskSketchManagerView::saveText()
{
	auto data = myModel->getModel()->getItem(currentIndex);
	data[INTERFACE(IUserTaskNotesDataExtention)]["notes"] = ui->textEdit->toPlainText();
	myModel->getModel()->updateItem(currentIndex, data);
	ui->pushButtonAdd->setEnabled(false);
	ui->pushButtonRevert->setEnabled(false);
}

void TaskSketchManagerView::revertText()
{
	reloadText();
}

void TaskSketchManagerView::onTreeViewClicked(const QModelIndex &index)
{
	currentIndex = m_filterUserTasks->mapToSource(index);
	reloadText();
}

void TaskSketchManagerView::reloadText()
{
	auto data = myModel->getModel()->getItem(currentIndex);
	auto name = data[INTERFACE(IUserTaskDataExtention)]["name"].toString();
	auto text = data[INTERFACE(IUserTaskNotesDataExtention)]["notes"].toString();

	ui->label->setText(QString("%1").arg(name));
	ui->textEdit->setText(text);
	ui->pushButtonAdd->setEnabled(false);
	ui->pushButtonRevert->setEnabled(false);
}
